

class Point{
	constructor(x,y){
		this.x = x;
		this.y = y;
	}

	distanceTo(point){
		let dx = point.x - this.x;
		let dy = point.y - this.y;
		return Math.sqrt(dx*dx + dy*dy);
	}

	move(x,y){
		this.x += x;
		this.y += y;
	}
}


let dragTarget = null;
let pizza;

function setDragTargetPos(point){
	let rect = dragTarget.getBoundingClientRect();
	point.move(-rect.width/2, -rect.height/2);
	dragTarget.style.left = point.x + "px";
	dragTarget.style.top = point.y + "px";
}

function distanceBetweenTarget(x1,y1,x2,y2){
	let dx = x1-x2;
	let dy = y1-y2;
	return Marh.sqrt(dx*dx + dy*dy);
}

function getPosOnPizza(mousePos){
	let rect = pizza.getBoundingClientRect();
	return new Point(mousePos.x-rect.left, mousePos.y-rect.y)
}

function getPizzaCenter(x,y){
	let rect = pizza.getBoundingClientRect();
	let cx = rect.left+rect.width/2;
	let cy = rect.top+rect.height/2;
	return new Point(cx,cy);
}

function getPizzaRadius(x,y){
	let rect = pizza.getBoundingClientRect();
	return rect.width/2;
}




document.addEventListener('DOMContentLoaded',()=>{

	pizza = document.querySelector(".pizza");

	window.addEventListener("mousedown", e =>{
		let mousePoint = new Point(e.clientX,e.clientY);
		if (e.target.matches(".x")) {
			dragTarget = e.target.cloneNode(true);
			dragTarget.style.position = 'fixed';
			document.body.append(dragTarget);
			setDragTargetPos(mousePoint);

		}

	});

	window.addEventListener('mousemove', e=>{
		if (dragTarget) {
			setDragTargetPos(new Point(e.clientX, e.clientY));
		}
	});


	window.addEventListener('mouseup', e=>{
		if (dragTarget) {


			let pizzaCenter = getPizzaCenter();
			let pizzaRadius = getPizzaRadius();
			let mousePos = new Point(e.clientX, e.clientY);
			let d = pizzaCenter.distanceTo(mousePos);

			if (d < pizzaRadius) {
				let newPos = getPosOnPizza(mousePos);
				setDragTargetPos(newPos);
				pizza.append(dragTarget);
				dragTarget.style.position='absolute';
				dragTarget = null;
			}
			else{
				dragTarget.remove();
				dragTarget = null;
			}
		}
	});
});







const inputsCheckbox = document.querySelectorAll('.menu'),
	  totalAmount = document.querySelector('.total-amount'),
	  orderbtn = document.getElementById('result'),
	  modalWindow = document.querySelector('.modal'),
	  closeModal = document.getElementById('clear'),
	  checkout = document.getElementById('checkout');

const subject = document.querySelector('.modal-window-subject'),
	  ingredientsSpan = document.querySelector('.modal-window-ingredients');




const addIngredients = checkboxes => {
	const nodesArray = Array.from(checkboxes);
	// const ingredientsArray = Array.from(ingredients);

	for(let node of checkboxes){
		node.addEventListener('click', event => {
			event.target.classList.toggle('active');
			const index = nodesArray.indexOf(event.target);
			// ingredientsArray[index].classList.toggle('active');
			calculateOrder();
		});
	}
}
addIngredients(inputsCheckbox);




const calculateOrder = () =>{
	const ingredients = document.querySelectorAll('.menu.active');

	const startPrice = 5;
	const ingredientsPrice = [].reduce.call( ingredients, function(res, dt) {
            return res += parseFloat(dt.dataset.cost * 1);
     },5);

	totalAmount.innerHTML = `${ingredientsPrice} ₴`;
}

 function clearCheckbox(){
	let uncheck = document.getElementsByTagName('input');

	for(let i=0; i<uncheck.length; i++){
		if (uncheck[i].type == 'checkbox') {
			uncheck[i].checked = false;
		}
	}
}



orderbtn.addEventListener('click', ()=>{
	modalWindow.classList.remove('none');
	prepareWindowModalContent();
});


closeModal.addEventListener('click', ()=>{
	modalWindow.classList.add('none');

});

checkout.addEventListener('click', ()=>{
	modalWindow.classList.add('none');
	location.reload();
});

window.addEventListener('click', event => {
	if (event.target == modalWindow) {
		modalWindow.classList.add('none');
	}
});




const prepareWindowModalContent = () =>{

	subject.innerHTML = '';
	ingredientsSpan.innerHTML = '';

	const addedIngredients = document.querySelectorAll('.menu.active');
	let ingredientList = [];
	let ingredientListCost = [];


	// ingredientList
	if (addedIngredients) {
		for(let ingredient of addedIngredients){
			ingredientList.push(ingredient.dataset.name);
		}
	}
	const totalIngredientsName = ingredientList.join('<br>  ') || 'no engredients';
	

	// ingredientListCost
	if (addedIngredients) {
		for(let ingredient of addedIngredients){
			ingredientListCost.push(ingredient.dataset.cost);
		}
	}
	const ingredientsCost = ingredientListCost.join('<br> ') || ' ';

	const totalText = `<div class="modal-wrapper">
					<div class="modal-title">
						<div class="title-item">Наименование</div>
						<div class="title-item">Стоимость</div>
						<div class="title-item">Сумма</div>
					</div>

					<div class="title-item ingridients">Ингредиенты</div>


					<div class="table">
						<div class="table-content">
							<div class="title-item">Тесто</div>
							<div class="title-item costs">5</div>
							<div class="title-item results">5</div>
						</div>
						<div class="table-content">
							<div class="title-item">${totalIngredientsName}</div>
							<div class="title-item costs">${ingredientsCost}</div>
							<div class="title-item results">${ingredientsCost}</div>
						</div>
					</div>

					<div class="modal-result">
						<div class="result">Итого</div>
						<div class="result"><span class="cost" id="cost">${totalAmount.innerHTML}</span></div>
					</div>
				</div>`;
	subject.innerHTML = totalText;
}


